remote_state {
  backend = "s3"
  config = {
    bucket = "${get_aws_account_id()}-terraform-state"

    key = "${path_relative_to_include()}/terraform.tfstate"
    region         = "eu-west-1"
    encrypt        = true
    dynamodb_table = "${get_aws_account_id()}-terragrunt-lock-table"
  }
}

skip = true
