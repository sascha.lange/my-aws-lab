include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::https://gitlab.com/sascha.lange/terragrunt-aws-route53.git?ref=v0.1.0"
}

inputs = {
  region         = "eu-west-1"
  environment    = "dev"
  product        = "lab"

  primary_domain = "dev.sl.gdnlab.io"
}
