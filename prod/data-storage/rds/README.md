# RDS

## ToDo

- Get password from secret store or parameter store
- Allow multiple read replicas
- Make DB creation optional
- Allow Read and Failover Replicas
