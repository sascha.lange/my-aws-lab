include {
  path     = find_in_parent_folders()
}

terraform {
  source = "git::https://gitlab.com/sascha.lange/terragrunt-aws-rds.git?ref=v0.1.1"
}

inputs = {
  region                  = "eu-west-1"
  environment             = "dev"
  product                 = "lab"

  engine                  = "mysql"
  allocated_storage       = 5
  multi_az                = false
  replica_count           = 0

  parameter_store_key_prefix = path_relative_to_include()

  apply_immediately       = true
  deletion_protection     = false
  skip_final_snapshot     = true

  parameters = [
    {
      name  = "character_set_client"
      value = "utf8"
    },
    {
      name  = "character_set_server"
      value = "utf8"
    },
    {
      name  = "max_connections"
      value = "512"
    }
  ]
}

