include {
  path     = find_in_parent_folders()
}

terraform {
  source = "git::https://gitlab.com/sascha.lange/terragrunt-aws-vpc.git?ref=v0.1.0"
}

inputs = {
  region      = "eu-west-1"
  environment = "dev"
  product     = "lab"

  vpc_cidr = "172.16.0.0/16"
  vpc_enable_nat_gateway = true
}
